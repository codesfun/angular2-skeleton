# GSS Angular2 Skeleton

## Requirements

- Node and NPM

## Installation

Run the following command from a bash shell or terminal window:

```bash

node setup

```

This will install all the project dependencies for both the Client and Server projects.


## Getting Started

View the README.md files located in the 'client' and 'server' sub-directories for more details.
