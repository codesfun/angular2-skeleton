/**
 * Created by gbaker on 11/16/16.
 */
var express = require('express');
var authService = require('../../services/authService/authService');
var response = require('../../services/responseService/responseService');
var router = express.Router();

router.post('/login',function(req, res, next) {

  var auth = new authService();

  console.log('req.body: ' + JSON.stringify(req.body));

  auth.authorize(req.body.username, req.body.password)
    .then(function(data){

        var r = new response();

        res.status(200).json(r.getResponse(data));
        next();
    })
    .catch(function(err){

        console.log('err: ' + err);

        var r = new response();

        res.status(401).json(r.getError('Invalid'));
        next();
    });
});

module.exports = router;
