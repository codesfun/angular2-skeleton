/**
 * Created by gbaker on 11/30/16.
 */

"use strict";

var express = require('express');
var dataService = require('../../services/dataService/dataService');
var response = require('../../services/responseService/responseService');
var router = express.Router();

router.get('/',function(req, res, next) {

  var data = new dataService();

  data.runSQL('SELECT * FROM gss_enviroment')
  .then(function(result){

      var r = new response();

      res.status(200).json(r.getResponse(result.Rows));
      next();
  })
  .catch(function(err){

      var r = new response();

      console.log(err);
      res.status(500).json(r.getError(err));
      next();
  });
});

router.post('/', function(req, res, next){

    var data = new dataService();

    var sql = "SET @current_php_user_name = 'limsabc';";
    sql = sql + "INSERT INTO gss_enviroment (name) VALUES ('GOOFY')";

    data.runSQL(sql)
      .then(function(result){

          var r = new response();

          res.status(200).json(r.getResponse('SUCCESS'));
          next();
      })
      .catch(function(err){

          var r = new response();

          res.status(500).json(r.getError('ERROR'));
          next();
      });
});

module.exports = router;
