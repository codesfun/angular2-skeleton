/**
 * Created by gbaker on 12/5/16.
 */

"use strict";

var express = require('express');
var queryService = require('../../services/queryService/queryService');
var response = require('../../services/responseService/responseService');
var router = express.Router();

router.get('/queries', function(req, res, next){

    var qs = new queryService();

    qs.getActiveQueries()
        .then(function(data){

            var r = new response();
            res.status(200).json(r.getResponse(data));
            next();
        })
        .catch(function(err){

            var r = new response();

            console.log(err);
            res.status(500).json(r.getError(err));
            next();
        });
});

module.exports = router;
