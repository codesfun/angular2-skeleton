/**
 * Created by gbaker on 12/5/16.
 */

/**
 * Created by gbaker on 12/5/16.
 */

"use strict";

var express = require('express');
var userService = require('../../services/userService/userService');
var response = require('../../services/responseService/responseService');
var router = express.Router();

router.get('/users', function(req, res, next){

    var us = new userService();

    us.getAllUsers()
        .then(function(data){

            var r = new response();
            res.status(200).json(r.getResponse(data));
            next();
        })
        .catch(function(err){

            var r = new response();

            console.log(err);
            res.status(500).json(r.getError(err));
            next();
        });
});

module.exports = router;

