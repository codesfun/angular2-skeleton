/**
 * Created by gbaker on 11/30/16.
 */

var mySQL = require('mysql');
var config = require('../config');

var gssMySQL = function() {

    "use strict";

    // the object
    var _obj = {};

    // PRIVATE
    var _pool = mySQL.createPool({
        connectionLimit: config.database.connectionLimit,
        host: config.database.host,
        user: config.database.username,
        database: config.database.database,
        password: config.database.password,
        multipleStatements: config.database.multipleStatements,
        ssl: config.database.ssl
    });

    var _sqlResult = function() {

        return {
          Success: true,
          Error: '',
          Rows: []
        }
    };


    // PUBLIC

    _obj.runSQL = function(sql) {

        var p = new Promise(function(resolve, reject){

          _pool.getConnection(function(err, conn){

            if(err) {

              result = new _sqlResult();
              result.Success = false;
              result.Error = "Unable to create a connection to the database.";

              reject(result);
            }
            else {

              try {

                conn.query(sql, function (err, rows) {

                  conn.release();

                  var result = new _sqlResult();

                  if (err) {

                    result.Success = false;
                  }
                  else {

                    result.Rows = rows;
                  }

                    resolve(result);
                });
              }
              catch (ex) {

                conn.release();

                var result = new _sqlResult();
                result.Success = false;

                  reject(result);
              }
            }
          });
        });

        return p;
    };

    _obj.runSP = function(sp, params) {

    };

    _obj.end = function() {

        _pool.end(function(err) {

        });

        console.log('Closed the connection pool.');
    };

    return _obj;
}();

module.exports = gssMySQL;
