/**
 * Created by gbaker on 12/2/16.
 */

"use strict";

var crypto = require('crypto');

var cryptoService = function () {

    var _obj = {};

    var _crypto = crypto;
    var _hashes = _crypto.getHashes();

    //private functions
    function _getHash(val, algorithm) {

        var hash = null;

        if(_hashes.includes(algorithm)) {

            hash = _crypto.createHash(algorithm);
            hash.update(val);
            return hash.digest('hex');
        }
        else {

            throw('Unsupported hash algorithm: ' + algorithm);
        }
    }


    //public functions
    _obj.getSHA1 = function(val) {

        try {

            return _getHash(val, 'sha1');
        }
        catch(err) {

            throw(err);
        }
    };

    _obj.getSHA256 = function(val) {

        try {

            return _getHash(val, 'sha256');
        }
        catch(err) {

            throw(err);
        }
    };

    _obj.getSHA512 = function(val) {

        try {

            return _getHash(val, 'sha512');
        }
        catch(err) {

            throw(err);
        }
    };

    _obj.getHash = function(val, algorithm) {

        try {

            return _getHash(val, algorithm);
        }
        catch(err) {

            throw(err);
        }
    };


    return _obj;
};

module.exports = cryptoService;
