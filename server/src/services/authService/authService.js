/**
 * Created by gbaker on 11/16/16.
 */

"use strict";

var jwt             = require('jsonwebtoken');
var cryptoService   = require('../cryptoService/cryptoService');
var dataService     = require('../dataService/dataService');
var config          = require('../../config');

var authService = function () {

  var _obj = {};

  var _jwt = jwt;
  var _dataService = new dataService();
  var _cryptoService = new cryptoService();
  var _config = config;

  //private functions
  function _authorize(username, pwd) {

      var p = new Promise(function(resolve, reject){

          var pwdHash = _cryptoService.getHash(pwd, config.auth.hashAlgorithm);
          var sql = "SELECT id, username, name, email FROM user WHERE username = '" + username + "' AND password = '" + pwdHash + "'";

          _dataService.runSQL(sql)
            .then(function(res){

                if(res.Rows.length > 0) {

                    var row = res.Rows[0];
                    var jwt = _createToken(row.id, row.name, row.username, row.email);

                    resolve(jwt);
                }
                else {

                    reject();
                }
            })
            .catch(function(err){

                reject(err);
            });
      });

      return p;
  }

  function _createToken(id, name, username, email) {

      var obj = {
          id: id,
          username: username,
          name: name,
          email: email
      };

      var jwt = _jwt.sign(obj, _config.secret, { algorithm: _config.jwt.algorithm, expiresIn: _config.jwt.expiresIn });
      obj.token = jwt;

      return obj;
  }

  function _validateJWT(token) {

      var p = new Promise(function(resolve, reject) {

          _jwt.verify(token, _config.secret, { algorithms: [_config.jwt.algorithm] }, function(err, decoded) {

              if(err) {

                  reject('Invalid.');
              }
              else {

                  resolve(decoded);
              }
          });
      });

      return p;
  }

    //public functions
    _obj.authorize = function(username, pwd) {

        var p = new Promise(function(resolve, reject) {

            _authorize(username, pwd)
                .then(function(jwt) {

                    resolve(jwt);
                })
                .catch(function(err) {

                    reject(false);
                });
        });

        return p;
    };

    _obj.validateToken = function(token) {

        var p = new Promise(function(resolve, reject) {

            _validateJWT(token)
                .then(function(res) {

                    resolve(res);
                })
                .catch(function(err){

                    reject(err);
                });
        });

        return p;
    };

    return _obj;
};

module.exports = authService;
