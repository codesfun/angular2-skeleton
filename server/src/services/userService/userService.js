/**
 * Created by gbaker on 12/5/16.
 */

"use strict";

var dataService = require('../dataService/dataService');

var userService = function () {

    var _obj = {};

    var _dataService = new dataService();

    //
    // PRIVATE
    //
    function _getData(sql) {

        var p = new Promise(function(resolve, reject) {

            _dataService.runSQL(sql)
                .then(function(data){

                    resolve(data);
                })
                .catch(function(err){

                    reject(err);
                });
        });

        return p;
    }

    //
    // PUBLIC
    //
    _obj.getAllUsers = function() {

        var sql = "SELECT * FROM user";
        return _getData(sql);
    };

    _obj.getUser = function(id) {

        var sql = "SELECT * FROM user WHERE id = " + id;
        return _getData(sql);
    };

    return _obj;
};

module.exports = userService;

