/**
 * Created by gbaker on 11/16/16.
 */

var responseService = function() {

  var _obj = {};

  //private functions


  //public functions
  _obj.getResponse = function(data) {

      return {
          Data: data,
          Error: false
      };
  };

  _obj.getError = function(errMsg) {

      return {
        Data: {
            ErrorMsg: errMsg,
            Error: true
          }
      };
  };

  return _obj;

};

module.exports = responseService;
