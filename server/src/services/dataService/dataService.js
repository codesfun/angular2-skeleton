/**
 * Created by gbaker on 11/30/16.
 */

var gssMySQL = require('../../data/mysql');

var dataService = function() {

    "use strict";

    var _obj = {};

    var _mySQL = gssMySQL;

    // PUBLIC
    _obj.runSQL = function(sql) {

        var p = new Promise(function(resolve, reject) {

            _mySQL.runSQL(sql)
            .then(function(res) {

                resolve(res);
            })
            .catch(function(err) {

                reject('Unable to retrieve data.');
            });
        });

        return p;
    };

    return _obj;
};

module.exports = dataService;
