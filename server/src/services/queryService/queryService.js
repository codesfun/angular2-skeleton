/**
 * Created by gbaker on 12/5/16.
 */

"use strict";

var dataService     = require('../dataService/dataService');

var queryService = function () {

    var _obj = {};

    var _dataService = new dataService();

    //
    // PRIVATE
    //
    function _getData(sql) {

        var p = new Promise(function(resolve, reject) {

            _dataService.runSQL(sql)
                .then(function(data){

                    resolve(data);
                })
                .catch(function(err){

                    reject(err);
                });
        });

        return p;
    }

    //
    // PUBLIC
    //
    _obj.getAllQueries = function(username, pwd) {

        var sql = "SELECT * FROM gss_Query";
        return _getData(sql);
    };

    _obj.getActiveQueries = function() {

        var sql = "SELECT * FROM gss_Query WHERE is_active = 1";
        return _getData(sql);
    };

    _obj.getQuery = function(id) {

        var sql = "SELECT * FROM gss_Query WHERE id = " + id;
        return _getData(sql);
    };

    return _obj;
};

module.exports = queryService;

