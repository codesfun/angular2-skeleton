// =================================================================
// get the packages we need
// =================================================================
"use strict"

var express 	= require('express');
var app         = express();
var bodyParser  = require('body-parser');
var morgan      = require('morgan');
var mysql       = require('mysql');
var jwt         = require('jsonwebtoken'); // used to create, sign, and verify tokens
var config      = require('./config'); // get our config file
var mySQL       = require('./data/mysql');
var authService = require('./services/authService/authService');
var responseService = require('./services/responseService/responseService');

// =================================================================
// configuration
// =================================================================
var port = process.env.PORT || 8080; // used to create, sign, and verify tokens

app.set('superSecret', config.secret); // secret variable

// use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// use morgan to log requests to the console
app.use(morgan('dev'));


// =================================================================
// HEADERS
// =================================================================
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


// =================================================================
// PUBLIC ROUTES
// =================================================================
var routeAuth = require('./routes/public/authRoute');
var routeEnv = require('./routes/public/envRoute');

app.use('/auth', routeAuth);
app.use('/env', routeEnv);

var apiRoutes = express.Router();

// ---------------------------------------------------------
// route middleware to authenticate and check token
// ---------------------------------------------------------
apiRoutes.use(function(req, res, next) {

	// check header or url parameters or post parameters for token
	var token = req.body.token || req.params.token;
    var auth = new authService();

    auth.validateToken(token)
        .then(function(decodedJwt) {

            req.decodedJwt = decodedJwt;
            next();
        })
        .catch(function(err) {

            var r = new responseService();
            return res.status(401).json(r.getError(err));
        });
});

// ---------------------------------------------------------
// PRIVATE ROUTES
// ---------------------------------------------------------
var queryRoutes = require('./routes/private/queryRoute');
var adminRoutes = require('./routes/private/adminRoute');

app.use('/api/query', queryRoutes);
app.use('/api/admin', adminRoutes);


// =================================================================
// start the server ================================================
// =================================================================
app.listen(port);
console.log('Magic happens at http://localhost:' + port);

// EXIT GRACEFULLY
process.on('SIGINT', function(){

    mySQL.end();
    console.log('Exiting node.js');
    process.exit();
});
