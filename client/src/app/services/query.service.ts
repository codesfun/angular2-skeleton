/**
 * Created by gbaker on 12/5/16.
 */

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { GSSApiService } from './api.service';
import { GSSLoginService } from './login.service';

@Injectable()
export class GSSQueryService {

    constructor ( private _api :GSSApiService, private _router :Router, private _loginService :GSSLoginService ) {

    }

    public getAllQueries() :void {

    }

    public getQuery(id) :void {

    }

    public addQuery() :void {

    }

    public saveQuery(id) :void {

    }


}