/**
 * Created by gbaker on 11/17/16.
 */

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { GSSApiService } from './api.service';

@Injectable()
export class GSSLoginService {

  constructor ( private _api: GSSApiService, private _router: Router ) {

  }

  login(username: string, pwd: string) {

      var self = this;

      var p = new Promise(function(resolve, reject){

          self._api.login(username, pwd)
              .then(function(data){

                  var res = JSON.parse(data._body);
                  window.localStorage.setItem('token', JSON.stringify(res.Data));

                  resolve(data);
              })
              .catch(function(err){

                  reject(err);
              });
      });

      return p;
  }

  logout() {

      window.localStorage.setItem('token', '');
      this._router.navigate(['/']);
  }

  getUser() {

      var user = {
          username: '',
          name: '',
          email: ''
      };

      var token = window.localStorage.getItem('token');

      if(token !== undefined && token !== null && token !== '') {

          token = JSON.parse(token);
          user.username = token.username;
          user.name = token.name;
          user.email = token.email;
      }
      else {

          this.logout();
      }

      return user;
  }
}
