import { Component } from '@angular/core';
import { GSSLoginService } from '../services/login.service';
import { Router } from '@angular/router';

/**
*	This class represents the lazy loaded LoginComponent.
*/

@Component({
	moduleId: module.id,
	selector: 'login-cmp',
	templateUrl: 'login.component.html'
})

export class LoginComponent {

    public _model = {
        username: '',
        pwd: ''
    };

    public alerts:Array<Object> = [];

    private _alertTimeout: number = 0;

    constructor( private _loginService: GSSLoginService, private _router: Router ) {

    }

    //
    // PRIVATE
    //

    private _gotoDashboard() {

        this._router.navigate(['/dashboard/home']);
    }

    private _gotoSignup() {

        this._router.navigate(['/signup']);
    }

    private _addAlert(msg :string) {

        this._clearAlerts(this);
        this.alerts.push({ type: 'danger', msg: msg, closable: true });
        this._alertTimeout = window.setTimeout(this._clearAlerts, 3000, this);
    }

    private _clearAlerts(self :LoginComponent) {

        if(self !== undefined && self !== null) {

            if(self._alertTimeout !== 0) {

                window.clearTimeout(self._alertTimeout);
                self._alertTimeout = 0;
            }

            if(self.alerts.length > 0) {

                self.alerts.splice(0, 1);
            }
        }
    }

    //
    // PUBLIC
    //

    login() {

        var self = this;

        this._loginService.login(this._model.username, this._model.pwd)
          .then(function(data){

              self._clearAlerts(self);
              self._gotoDashboard();
          })
          .catch(function(err){

              self._addAlert('Invalid credentials.');
          });
    }

    closeAlert() {

        this._clearAlerts(this);
    }

    signup() {

        this._gotoSignup();
    }
}
