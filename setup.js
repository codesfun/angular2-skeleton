/**
 * Created by gbaker on 11/30/16.
 */

var cp = require('child_process');

var clientPackage = __dirname + '/client';
var serverPackage = __dirname + '/server';

console.log(clientPackage);
console.log(serverPackage);

// install folder
cp.spawnSync('npm', ['i'], { env: process.env, cwd: clientPackage, stdio: 'inherit' });
cp.spawnSync('npm', ['i'], { env: process.env, cwd: serverPackage, stdio: 'inherit' });
